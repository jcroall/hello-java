pipeline {
	agent any

	environment {
    PATH = "/usr/local/bin:${env.PATH}"

    // Mimic variables coming in from GitLab
    GITLAB_TOKEN = 'glpat-5fjfd6m7fiHmqykNiemb' // No equivilant in gitlab plugin, must be hard-coded
    CI_SERVER_URL= 'https://gitlab.com' // No equivilant in gitlab plugin, must be hard-coded
    CI_PROJECT_NAMESPACE = "jcroall" // Equivilant to gitlabSourceNamespace
    CI_COMMIT_BRANCH = "${GIT_BRANCH.split("/")[1]}" // Equivilant to gitlabSourceBranch
    CI_COMMIT_SHA = "" // Equivilant to gitlabTargetBranch, will be set dynamically   below
    CI_PROJECT_NAME = "hello-java" // I *think* equivilant to gitlabSourceRepoName
    CI_PROJECT_ID = "" // Must get programmatically, see below

    COVERITY_URL = 'http://local-coverity:8080'
    COVERITY_TOOL_HOME = '/Users/jcroall/Applications/cov-analysis-macosx-2021.12.1'
    COVERITY_STREAM_NAME = "${env.CI_PROJECT_NAME}-${env.CI_COMMIT_BRANCH}"
    COVERITY_STREAM_NAME_CHANGE = "${env.CI_PROJECT_NAME}-${env.CHANGE_TARGET}"
    COVERITY_PROJECT_NAME = "${env.CI_PROJECT_NAME}"

    BRANCH_NAME = "${GIT_BRANCH.split("/")[1]}"
	}

	tools {
		jdk 'jdk9'
	}

	stages {
		stage('Build') {
			steps {
				sh 'mvn -B compile'
			}
		}
		stage('Test') {
			steps {
				sh 'mvn -B test'
			}
		}
		stage('Coverity Full Scan') {
			when {
				allOf {
					not { changeRequest() }
					expression { BRANCH_NAME ==~ /(main|master|stage|release)/ }
				}
			}
			steps {
				withCoverityEnvironment(coverityInstanceUrl: "$COVERITY_URL", projectName: "$CI_PROJECT_NAME", streamName: "$COVERITY_STREAM_NAME") {
					sh '''
						cov-build --dir idir --fs-capture-search $WORKSPACE mvn -B clean package -DskipTests
						cov-analyze --dir idir --ticker-mode none --strip-path $WORKSPACE --webapp-security
						cov-commit-defects --dir idir --ticker-mode none --url $COV_URL --stream $COV_STREAM \
							--description $BUILD_TAG --version $GIT_COMMIT
					'''
					script { // Coverity Quality Gate
						count = coverityIssueCheck(viewName: 'OWASP Web Top 10', returnIssueCount: true)
						if (count != 0) { unstable 'issues detected' }
					}
				}
			}
		}
		stage('Coverity Incremental Scan') {
			when {
				allOf {
					changeRequest()
					expression { CHANGE_TARGET ==~ /(master|stage|release)/ }
				}
			}
			steps {
				withCoverityEnvironment(coverityInstanceUrl: "$COVERITY_URL", projectName: "$CI_PROJECT_NAME", streamName: "$COVERITY_STREAM_NAME_CHANGE") {
//          withCoverityEnvironment(coverityInstanceUrl: "$COVERITY_URL", projectName: "$CI_PROJECT_NAME", streamName: "$CI_PROJECT_NAME-$CHANGE_TARGET") {
					sh '''
						export CHANGE_SET=$(git --no-pager diff origin/$CHANGE_TARGET --name-only)
						[ -z "$CHANGE_SET" ] && exit 0
						cov-run-desktop --dir idir --url $COV_URL --stream $COV_STREAM --build mvn -B clean package -DskipTests
						cov-run-desktop --dir idir --url $COV_URL --stream $COV_STREAM --present-in-reference false \
							--ignore-uncapturable-inputs true --text-output issues.txt $CHANGE_SET
						if [ -s issues.txt ]; then cat issues.txt; touch issues_found; fi
					'''
				}
				script { // Coverity Quality Gate
					if (fileExists('issues_found')) { unstable 'issues detected' }
				}
			}
		}
		stage('Deploy') {
			when {
				expression { BRANCH_NAME ==~ /(master|stage|release)/ }
			}
			steps {
				sh 'mvn -B install'
			}
		}
	}
  stage('gitlab') {
    steps {
      echo 'Notify GitLab'
      updateGitlabCommitStatus name: 'build', state: 'pending'
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }

	post {
		always {
			cleanWs()
		}
	}
  }
}
